# test.py
# Nikko Pante
# GmE 205: Diagnostic Test and Introduction
# run: python -m unittest test

import unittest


# Batman and Robin catch villains.
# How many villains did they imprisoned?

class Villain(object):
	def __init__(self, name, age):
		self.name = name
		self.age = age

	def punch(self):
		return("Kapow!")

	def shoot(self):
		return("Bang!")


class Superhero(object):
	def __init__(self, name):
		self.name = name
		self.catched = 0
		self.catch_list = []

	def dodge(self):
		return("Swing!")

	def catch(self, villain):
		self.catched = self.catched + 1
		self.catch_list.append(villain.name)
		return("Zap!")



class TestStory(unittest.TestCase):

	def test_batman_and_robin_saves_the_day(self):
		joker = Villain("Joker", 25)
		mr_freeze = Villain("Mr. Freeze", 25)
		penguin = Villain("Penguin", 35)

		batman = Superhero("Batman")
		robin = Superhero("Robin")

		joker.punch()
		batman.dodge()

		batman.catch(joker)
		robin.catch(mr_freeze)
		robin.catch(penguin)

		self.assertEqual(batman.catched, 1)
		self.assertEqual(robin.catched, 2)

		self.assertEqual(batman.catch_list, ["Joker"])
		self.assertEqual(robin.catch_list, ["Mr. Freeze", "Penguin"])

		total_catch = batman.catched + robin.catched

		print("Batman and Robin saves the day from", total_catch, "villains.")

		

if __name__ == '__main__':
    unittest.main()